package ru.alpatov.SpringShop.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.repositories.AccountsRepository;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Viacheslav Alpatov
 */
@SpringBootTest
class CartServiceTest {

    @Autowired
    CartService cartService;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private AccountsRepository accountsRepository;
    private Account account;

    @BeforeEach
    public void setup(){
        account = Account.builder()
                .name("testName")
                .lastName("testLastName")
                .email("t@t.ru")
                .password(passwordEncoder.encode("testPasswd"))
//                .yearOfBirth(1975)
                .role("ROLE_USER")
                .build();
        accountsRepository.save(account);
    }
    @Test
    @Transactional
    void addBookToCartByAccountTest() {
        cartService.addBookToCartByAccount(account,1L);
        cartService.addBookToCartByAccount(account,2L);
        List<Book> books = cartService.getBookInCartByAccount(account);
        assertEquals (books.size(),2);
    }
}