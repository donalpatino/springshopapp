package ru.alpatov.SpringShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;


/**
 * @author Viacheslav Alpatov
 */
@SpringBootApplication
public class SpringShopApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringShopApp.class, args);
    }


}
