package ru.alpatov.SpringShop.errors;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Viacheslav Alpatov
 */
@Data
@AllArgsConstructor
public class AccountErrorResponse {
    private String message;
    private long timestamp;

}
