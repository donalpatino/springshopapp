package ru.alpatov.SpringShop.errors;

/**
 * @author Viacheslav Alpatov
 */
public class NotCreatedException extends RuntimeException {
    public NotCreatedException(String msg) {
        super(msg);
    }
}
