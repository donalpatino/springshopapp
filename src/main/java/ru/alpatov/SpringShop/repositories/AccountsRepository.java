package ru.alpatov.SpringShop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alpatov.SpringShop.models.Account;

import java.util.Optional;

/**
 * @author Viacheslav Alpatov
 */
@Repository
public interface AccountsRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByEmail(String username);
}
