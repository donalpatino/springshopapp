package ru.alpatov.SpringShop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alpatov.SpringShop.models.Book;

import java.util.List;
import java.util.Optional;

/**
 * @author Viacheslav Alpatov
 */
@Repository
public interface BooksRepository extends JpaRepository<Book, Long> {
    @Override
    Optional<Book> findById(Long id);
    List<Book> findAllByPriceGreaterThan(int minPrice);
    List<Book> findByNameContains(String name);

}
