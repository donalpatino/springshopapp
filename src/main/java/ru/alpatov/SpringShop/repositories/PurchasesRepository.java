package ru.alpatov.SpringShop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.models.Purchase;
import java.util.List;

/**
 * @author Viacheslav Alpatov
 */
@Repository
public interface PurchasesRepository extends JpaRepository<Purchase, Long> {
   // List<Purchase> findAllByAccount(Account account);
    List<Purchase> findAllByAccountAndDateNotNull(Account account);
    List<Purchase> findAllByAccountAndDateNull(Account account);
}
