package ru.alpatov.SpringShop.controllers;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import ru.alpatov.SpringShop.dto.AccountCreationDto;
import ru.alpatov.SpringShop.dto.AccountDto;
import ru.alpatov.SpringShop.errors.AccountErrorResponse;
import ru.alpatov.SpringShop.errors.AccountNotFoundException;
import ru.alpatov.SpringShop.errors.NotCreatedException;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.services.AccountService;
import ru.alpatov.SpringShop.util.AccountValidator;

import javax.validation.Valid;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Viacheslav Alpatov
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private final ModelMapper modelMapper;
    private final AccountService accountService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private final AccountValidator accountValidator;

    @GetMapping()
    public List<AccountDto> getUsers() {
        List<Account> accounts = accountService.allUserList();
        return accounts.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public AccountDto getPerson(@PathVariable("id") int id) {
        return convertToDto(accountService.findOne(id)); // Jackson конвертирует в JSON
    }

    @PostMapping()
    public ResponseEntity<HttpStatus> create(@RequestBody @Valid AccountCreationDto accountCreationDto, BindingResult bindingResult) throws ParseException {

        accountValidator.validate(accountCreationDto, bindingResult);

        if (bindingResult.hasErrors()) {
            StringBuilder errorMsg = new StringBuilder();
            List<FieldError> errors = bindingResult.getFieldErrors();
            for (FieldError error : errors) {
                errorMsg.append(error.getField())
                        .append(" - ").append(error.getDefaultMessage())
                        .append(";");
            }
            throw new NotCreatedException(errorMsg.toString());
        }
        accountService.save(convertToEntity(accountCreationDto));

        return ResponseEntity.ok(HttpStatus.OK);
    }


    @ExceptionHandler
    private ResponseEntity<AccountErrorResponse> handleException(AccountNotFoundException e) {
        AccountErrorResponse response = new AccountErrorResponse(
                "Account not found",
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler
    private ResponseEntity<AccountErrorResponse> handleException(NotCreatedException e) {
        AccountErrorResponse response = new AccountErrorResponse(
                e.getMessage(),
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }

    private AccountDto convertToDto(Account account) {
        return modelMapper.map(account, AccountDto.class);
    }

    private Account convertToEntity(AccountCreationDto accountCreationDto) {
        Account account = modelMapper.map(accountCreationDto, Account.class);
        account.setPassword(passwordEncoder.encode(accountCreationDto.getPassword()));
        account.setRole("ROLE_USER");
        account.setCreatedAt(LocalDateTime.now());
        return account;
    }
}
