package ru.alpatov.SpringShop.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import ru.alpatov.SpringShop.dto.BookDto;
import ru.alpatov.SpringShop.errors.AccountErrorResponse;
import ru.alpatov.SpringShop.errors.AccountNotFoundException;
import ru.alpatov.SpringShop.errors.NotCreatedException;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.services.BookService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Viacheslav Alpatov
 */
@RestController // @Controller + @ResponseBody над каждым методом
@RequestMapping("/api/books")
public class BooksController {

    @Autowired
    BookService bookService;

    @Autowired
    ModelMapper modelMapper;

    //Передаём сюда что-то вот такое
    // http://localhost:8080/books/?books_per_page=5&page=4&sort_by_year=true
    @GetMapping()
    public List<BookDto> getBooks(                        @RequestParam(value = "page", required = false) Integer page,
                                                          @RequestParam(value = "books_per_page", required = false) Integer booksPerPage,
                                                          @RequestParam(value = "sort_by_year", required = false) boolean sortByYear) {
        List <Book> books;
        if (page != null && booksPerPage != null) {
            books =  bookService.findWithPagination(page, booksPerPage, sortByYear);
        }
        else
            books = bookService.findAll(sortByYear); // Jackson конвертирует эти объекты в JSON

        return books.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/ordered")
    public List<BookDto> getOrderedBooks() {
        List <Book> books = bookService.findAllBookWithCustomer(); // Jackson конвертирует эти объекты в JSON
        return books.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public BookDto getBook(@PathVariable("id") int id) {
        return convertToDto(bookService.findOne(id)); // Jackson конвертирует в JSON
    }

    @PostMapping()
    public ResponseEntity<HttpStatus> create(@RequestBody @Valid BookDto bookDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            StringBuilder errorMsg = new StringBuilder();
            List<FieldError> errors = bindingResult.getFieldErrors();
            for (FieldError error : errors) {
                errorMsg.append(error.getField())
                        .append(" - ").append(error.getDefaultMessage())
                        .append(";");
            }
            throw new NotCreatedException(errorMsg.toString());
        }
        bookService.save(convertToEntity(bookDto));

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ExceptionHandler
    private ResponseEntity<AccountErrorResponse> handleException (AccountNotFoundException e){
        AccountErrorResponse response = new AccountErrorResponse(
                "Book not found",
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler
    private ResponseEntity<AccountErrorResponse> handleException(NotCreatedException e) {
        AccountErrorResponse response = new AccountErrorResponse(
                e.getMessage(),
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }
    private BookDto convertToDto(Book book) {
        return modelMapper.map(book, BookDto.class);
    }
    private Book convertToEntity(BookDto bookDto) {
        return modelMapper.map(bookDto, Book.class);
    }
}
