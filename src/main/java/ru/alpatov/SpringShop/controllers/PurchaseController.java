package ru.alpatov.SpringShop.controllers;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.alpatov.SpringShop.dto.BookDto;
import ru.alpatov.SpringShop.dto.PurchaseDto;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.services.BookServiceImpl;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Viacheslav Alpatov
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class PurchaseController {
    @Autowired
    private final BookServiceImpl bookService;
    @Autowired
    private final ModelMapper modelMapper;

    @GetMapping("/purchases")
    public @ResponseBody List<BookDto> index(){
        List<Book> books =  bookService.findAllBookWithCustomer();
        return books.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }
    private BookDto convertToDto(Book book) {
        BookDto bookDto = modelMapper.map(book, BookDto.class);
        for (PurchaseDto purchaseDto: bookDto.getPurchases()
             ) {
            if (purchaseDto.getDate() == null) {
                purchaseDto.setDate("Товар в корзине");
            }
        }
        return bookDto;
    }
}
