package ru.alpatov.SpringShop.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.alpatov.SpringShop.models.Book;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


/**
 * @author Viacheslav Alpatov
 */
@Component
public class BookDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(readOnly = true)
    public List<Book> findOrderedBooks(){
        Session session = entityManager.unwrap(Session.class);
        List<Book> books = session.createQuery("select b from Book b join fetch b.purchases p join fetch p.account").getResultList();
        return books;
    }
}
