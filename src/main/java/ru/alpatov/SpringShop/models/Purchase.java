package ru.alpatov.SpringShop.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author Viacheslav Alpatov
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date date;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    //@OneToMany(fetch = FetchType.LAZY, mappedBy = "purchase")
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "purchase_book",
            joinColumns = {@JoinColumn(name = "purchase_id")},
            inverseJoinColumns = {@JoinColumn(name = "book_id")}
    )
    private List<Book> books;


    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", date='" + date + '\'' +
                '}';
    }
}