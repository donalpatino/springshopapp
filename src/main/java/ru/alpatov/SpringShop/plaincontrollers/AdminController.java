package ru.alpatov.SpringShop.plaincontrollers;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.alpatov.SpringShop.dto.AccountDto;
import ru.alpatov.SpringShop.dto.BookDto;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.repositories.BooksRepository;
import ru.alpatov.SpringShop.services.AccountService;
import ru.alpatov.SpringShop.services.BookService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Viacheslav Alpatov
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController {

    private final AccountService accountService;
    private final BooksRepository booksRepository;
    private final BookService bookService;
    @Autowired
    private final ModelMapper modelMapper;

    @GetMapping()
    public String index() {
        return "admin/index";
    }

    @GetMapping("/users")
    public String adminPage(Model model) {
        List<Account> accounts = accountService.allUserList();
        List<AccountDto> users = accounts.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
        model.addAttribute("users", users);
        return "admin/users";
    }

    private AccountDto convertToDto(Account account) {
        return modelMapper.map(account, AccountDto.class);
    }

    @GetMapping("/books")
    public String books(Model model) {
        model.addAttribute("books", booksRepository.findAll());
        return "admin/books";
    }

    @GetMapping("books/{id}")
    public String showBook(@PathVariable("id") long id, Model model) {
        Optional<Book> book = booksRepository.findById(id);
        book.ifPresent(value -> model.addAttribute("book", value));
        return "admin/showbook";
    }

    @GetMapping("books/new")
    public String newPerson(@ModelAttribute("book") BookDto bookDTO) {

        return "admin/newbook";

    }

    @PostMapping("books/new")
    public String createBook(@ModelAttribute("book") @Valid BookDto bookDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return "admin/newbook";
        bookService.add(bookDTO);
        return "redirect:/admin/books";
    }

    @GetMapping("books/edit/{id}")
    public String editBook(@PathVariable("id") long id, Model model) {
        Optional<Book> book = booksRepository.findById(id);
        book.ifPresent(value -> model.addAttribute("book", value));
        return "admin/editbook";
    }

    @PostMapping("books/edit/{id}")
    public String updateBook(@ModelAttribute("book") @Valid Book book, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return "admin/editbook";
        bookService.update(book);
        return "redirect:/admin/books/{id}";
    }

    @GetMapping("books/delete/{id}")
    public String deleteBook(@PathVariable("id") long id) {
        bookService.delete(id);
        return "redirect:/admin/books";
    }
}
