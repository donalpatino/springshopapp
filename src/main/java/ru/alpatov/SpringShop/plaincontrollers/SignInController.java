package ru.alpatov.SpringShop.plaincontrollers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Viacheslav Alpatov
 */

@Controller
@RequiredArgsConstructor
public class SignInController {
    @GetMapping("/signin")
    public String loginPage() {
        return "signin";
    }
}
