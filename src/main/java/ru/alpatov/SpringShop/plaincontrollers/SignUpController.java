package ru.alpatov.SpringShop.plaincontrollers;


import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.alpatov.SpringShop.dto.AccountCreationDto;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.services.SignUpService;
import ru.alpatov.SpringShop.util.AccountValidator;
import javax.validation.Valid;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Viacheslav Alpatov
 **/

@Controller
@RequiredArgsConstructor
public class SignUpController {

    @Autowired
    private final SignUpService signUpService;
    @Autowired
    private final AccountValidator accountValidator;
    @Autowired
    private final ModelMapper modelMapper;
    @Autowired
    PasswordEncoder passwordEncoder;
    @GetMapping("/signup")
    public String getSignUpPage(@ModelAttribute("person") AccountCreationDto accountCreationDto) {
        return "signup";
    }

    @PostMapping("/signup")
    public String signUp(@ModelAttribute("person") @Valid AccountCreationDto accountCreationDto, BindingResult bindingResult) throws ParseException {
        accountValidator.validate(accountCreationDto, bindingResult);

        if (bindingResult.hasErrors())
            return "/signup";

        Account account = convertToEntity(accountCreationDto);
        signUpService.signUp(account);
        //signUpService.signUp(accountCreationDto);
        return "redirect:/signin";
    }
    private Account convertToEntity(AccountCreationDto accountCreationDto) throws ParseException {
        Account account = modelMapper.map(accountCreationDto, Account.class);
        account.setPassword(passwordEncoder.encode(accountCreationDto.getPassword()));
        account.setCreatedAt(LocalDateTime.now());
        account.setRole("ROLE_USER");
        return account;
    }
}
