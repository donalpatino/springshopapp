package ru.alpatov.SpringShop.plaincontrollers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.alpatov.SpringShop.errors.AccountErrorResponse;
import ru.alpatov.SpringShop.errors.AccountNotFoundException;
import ru.alpatov.SpringShop.errors.NotCreatedException;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.repositories.BooksRepository;
import ru.alpatov.SpringShop.services.BookService;

import java.util.Optional;

/**
 * @author Viacheslav Alpatov
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {

    @Autowired
    BooksRepository booksRepository;

    @Autowired
    BookService bookService;

    //Передаём сюда что-то вот такое
    // http://localhost:8080/books/?books_per_page=5&page=4&sort_by_year=true
    @GetMapping()
    public String index(Model model,
                        @RequestParam(value = "page", required = false) Integer page,
                        @RequestParam(value = "books_per_page", required = false) Integer booksPerPage,
                        @RequestParam(value = "sort_by_year", required = false) boolean sortByYear,
                        @RequestParam (required = false) Integer filter,
                        @RequestParam (required = false) String search
//                        @RequestParam Optional<String> filter,
//                        @RequestParam Optional<String> search

    ) {
        if (page != null && booksPerPage != null){
            model.addAttribute("books", bookService.findWithPagination(page, booksPerPage, sortByYear));
            return "books/index";
        }
        if (filter != null)
        {
            model.addAttribute("books", booksRepository.findAllByPriceGreaterThan(filter));
            return "books/index";
        }
        if(search != null)
        {
            model.addAttribute("books", bookService.searchByTitle(search));
            return "books/index";
        }
        model.addAttribute("books", bookService.findAll(sortByYear));
        return "books/index";

 /*       int minPrice;
        if (filter.isEmpty() || filter.get().equals("")) {
            model.addAttribute("books", bookService.findAll(sortByYear));
        } else {
            minPrice = Integer.parseInt(filter.get());
            model.addAttribute("books", booksRepository.findAllByPriceGreaterThan(minPrice));
            return "books/index";
        }

        if (search.isEmpty() || search.get().equals("")) {
            model.addAttribute("books", bookService.findAll(sortByYear));
        } else {
            String searchStr = search.get();
            model.addAttribute("books", bookService.searchByTitle(searchStr));
            return "books/index";
        }

        return "books/index";

  */
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") long id, Model model) {
        Book book = bookService.findOne(id);
        model.addAttribute("book", book);
        return "books/show";
    }

    @GetMapping("/search")
    public String searchPage() {
        return "books/search";
    }

    @PostMapping("/search")
    public String doSearch(Model model, @RequestParam String query) {
        model.addAttribute("books", bookService.searchByTitle(query));
        return "books/search";
    }

    @ExceptionHandler
    private ResponseEntity<AccountErrorResponse> handleException(AccountNotFoundException e) {
        AccountErrorResponse response = new AccountErrorResponse(
                "Book not found",
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler
    private ResponseEntity<AccountErrorResponse> handleException(NotCreatedException e) {
        AccountErrorResponse response = new AccountErrorResponse(
                e.getMessage(),
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }
}
