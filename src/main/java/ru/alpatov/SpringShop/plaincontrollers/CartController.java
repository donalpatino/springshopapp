package ru.alpatov.SpringShop.plaincontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.security.AccountDetails;
import ru.alpatov.SpringShop.services.CartService;

import java.util.List;

/**
 * @author Viacheslav Alpatov
 */
@Controller
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class CartController {
    @Autowired
    CartService cartService;

    @GetMapping("/cart")
    public String cart(Model model) {
        List<Book> books = cartService.getBookInCartByAccount(getCurrentAccount());
        model.addAttribute("books",books);
        return "purchases/cart";
    }

    @GetMapping("/addBookToCart/{id}")
    public String addBookToCart(@PathVariable("id") long bookId) {
        cartService.addBookToCartByAccount(getCurrentAccount(), bookId);
        return "redirect:/cart";
    }

    @GetMapping("/cartToOrder")
    public String cartToOrder() {
        cartService.cartToOrder(getCurrentAccount());
        return "redirect:/purchases";
    }

    Account getCurrentAccount(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AccountDetails personDetails = (AccountDetails) authentication.getPrincipal();
        return personDetails.getAccount();
    }
}
