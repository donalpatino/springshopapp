package ru.alpatov.SpringShop.plaincontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.models.Purchase;
import ru.alpatov.SpringShop.repositories.AccountsRepository;
import ru.alpatov.SpringShop.repositories.PurchasesRepository;
import ru.alpatov.SpringShop.security.AccountDetails;
import ru.alpatov.SpringShop.services.CartService;
import ru.alpatov.SpringShop.services.OrdersService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Viacheslav Alpatov
 */
@Controller
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class HelloController {

    @Autowired
    AccountsRepository accountsRepository;
    @Autowired
    PurchasesRepository purchasesRepository;
    @Autowired
    OrdersService ordersService;
    @Autowired
    CartService cartService;

    @GetMapping("/hello")
    public String sayHello(Model model) {
        model.addAttribute("person", getCurrentAccount());
        return "hello";
    }

    @GetMapping("/purchases")
    public String purchases(Model model) {
        List<Purchase> orders = ordersService.getOrdersByAccount(getCurrentAccount());
        model.addAttribute("purchases", orders);
        return "purchases/purchases";
    }

    @GetMapping("/purchases/{id}")
    public String showPurchase(@PathVariable("id") long id, Model model) {
        List<Book> books = new ArrayList<>();
        Optional<Purchase> purchase = purchasesRepository.findById(id);
        if (purchase.isPresent())
            books = purchase.get().getBooks();
        model.addAttribute("books", books);
        return "purchases/show";
    }

    Account getCurrentAccount() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AccountDetails personDetails = (AccountDetails) authentication.getPrincipal();
        return personDetails.getAccount();
    }
}
