package ru.alpatov.SpringShop.services;

import ru.alpatov.SpringShop.dto.AccountCreationDto;
import ru.alpatov.SpringShop.models.Account;

/**
 * @author Viacheslav Alpatov
 */
public interface SignUpService {

    void signUp(AccountCreationDto accountCreationDto);

    void signUp(Account account);
}


