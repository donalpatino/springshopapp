package ru.alpatov.SpringShop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.repositories.AccountsRepository;
import ru.alpatov.SpringShop.security.AccountDetails;

import java.util.Optional;

/**
 * @author Viacheslav Alpatov
 */
@Service
@RequiredArgsConstructor
public class AccountDetailsService implements UserDetailsService {

    @Autowired
    private final AccountsRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<Account> account = accountRepository.findByEmail(s);

        if (account.isEmpty())
            throw new UsernameNotFoundException("User not found");

        return new AccountDetails(account.get());
    }
}
