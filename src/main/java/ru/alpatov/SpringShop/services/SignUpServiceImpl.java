package ru.alpatov.SpringShop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.alpatov.SpringShop.dto.AccountCreationDto;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.repositories.AccountsRepository;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Viacheslav Alpatov
 */

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    private final AccountsRepository accountsRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void signUp(AccountCreationDto accountCreationDto) {
        Account account = Account.builder()
                .name(accountCreationDto.getName())
                .lastName(accountCreationDto.getLastName())
                .email(accountCreationDto.getEmail())
                .password(passwordEncoder.encode(accountCreationDto.getPassword()))
                .dateOfBirth(accountCreationDto.getDateOfBirth())
                .createdAt(LocalDateTime.now())
                .role("ROLE_USER")
                .build();
        accountsRepository.save(account);
    }
    @Override
    public void signUp(Account account) {
        accountsRepository.save(account);
    }

}
