package ru.alpatov.SpringShop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.models.Purchase;
import ru.alpatov.SpringShop.repositories.PurchasesRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Viacheslav Alpatov
 */
@Service
@RequiredArgsConstructor
@Transactional
public class OrdersService {

    private final PurchasesRepository purchasesRepository;

    public List<Purchase> getOrdersByAccount(Account account) {
        return purchasesRepository.findAllByAccountAndDateNotNull(account);
    }
}
