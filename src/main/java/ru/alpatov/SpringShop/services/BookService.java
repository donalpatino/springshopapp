package ru.alpatov.SpringShop.services;

import ru.alpatov.SpringShop.dto.BookDto;
import ru.alpatov.SpringShop.models.Book;

import java.util.List;
import java.util.Optional;

/**
 * @author Viacheslav Alpatov
 */
public interface BookService {

    void add(BookDto bookDTO);
    void update(Book book);
    void delete(Long bookId);
    public List<Book> findAllBookWithCustomer();
    public List<Book> findAll(boolean sortByYear);
    public List<Book> findWithPagination(Integer page,Integer booksPerPage, boolean sortByYear);
    public List<Book> searchByTitle(String query);
    public Book findOne(long id);

    void save (Book book);
}


