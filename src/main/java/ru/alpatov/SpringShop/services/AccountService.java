package ru.alpatov.SpringShop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.alpatov.SpringShop.errors.AccountNotFoundException;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.repositories.AccountsRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @author Viacheslav Alpatov
 */
@Service
@RequiredArgsConstructor
public class AccountService {
    @Autowired
    AccountsRepository accountsRepository;


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Account> allUserList() {
        return accountsRepository.findAll();
    }

    public Account findOne(long id) {
        Optional<Account> foundAccount = accountsRepository.findById(id);
        //return foundAccount.orElse(null);
        return foundAccount.orElseThrow(AccountNotFoundException::new);
    }
    public Optional<Account> findByEmail (String email){
        return accountsRepository.findByEmail(email);
    }
    @Transactional
    public void save (Account account){
        //Проверить на уникальность e-mail в БД через AccountValidator

        accountsRepository.save(account);
    }
}

