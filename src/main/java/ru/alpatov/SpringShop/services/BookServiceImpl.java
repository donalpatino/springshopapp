package ru.alpatov.SpringShop.services;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.alpatov.SpringShop.dao.BookDao;
import ru.alpatov.SpringShop.dto.BookDto;
import ru.alpatov.SpringShop.errors.AccountNotFoundException;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.repositories.BooksRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author Viacheslav Alpatov
 */

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    @Autowired
    private final BooksRepository booksRepository;

    @Autowired
    private final BookDao bookDAO;


    @Override
    public void add(BookDto bookDTO) {
        //TODO проверка на уникальность книги
        Book book = Book.builder()
                .name(bookDTO.getName())
                .author(bookDTO.getAuthor())
                .yearOfPublish(bookDTO.getYearOfPublish())
                .price(bookDTO.getPrice())
                .build();
        booksRepository.save(book);
    }

    @Override
    public void update(Book book) {
        //TODO проверка на уникальность книги
        booksRepository.save(book);
    }

    @Override
    public void delete(Long bookId) {
        Optional<Book> book = booksRepository.findById(bookId);
        book.ifPresent(booksRepository::delete);
    }

    public List<Book> findAllBookWithCustomer() {
        return bookDAO.findOrderedBooks();

    }

    public List<Book> searchByTitle(String query) {
        return booksRepository.findByNameContains(query);

    }
    public List<Book> findAll(boolean sortByYear) {
        if (sortByYear)
            return booksRepository.findAll(Sort.by("yearOfPublish"));
        else
            return booksRepository.findAll();

    }

    public List<Book> findWithPagination(Integer page, Integer booksPerPage, boolean sortByYear) {
        if (sortByYear)
            return booksRepository.findAll(PageRequest.of(page, booksPerPage, Sort.by("yearOfPublish"))).getContent();
        else
            return booksRepository.findAll(PageRequest.of(page, booksPerPage)).getContent();
    }

    public Book findOne(long id) {
        Optional<Book> foundBook = booksRepository.findById(id);
        //return foundBook.orElse(null);
        return foundBook.orElseThrow(AccountNotFoundException::new);
    }

    @Transactional
    public void save(Book book) {
        booksRepository.save(book);
    }
}
