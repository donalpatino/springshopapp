package ru.alpatov.SpringShop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.models.Purchase;
import ru.alpatov.SpringShop.repositories.BooksRepository;
import ru.alpatov.SpringShop.repositories.PurchasesRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * @author Viacheslav Alpatov
 */
@Service
@RequiredArgsConstructor
@Transactional
public class CartService {

    private final PurchasesRepository purchasesRepository;
    @Autowired
    private final BooksRepository booksRepository;

    public List<Book> getBookInCartByAccount(Account account) {
        List<Purchase> carts = purchasesRepository.findAllByAccountAndDateNull(account);
        List<Book> books;
        if (carts.isEmpty())
            books = new ArrayList<>();
        else
            books = carts.get(0).getBooks();
        return books;
    }

    public void addBookToCartByAccount(Account account, Long bookId) {
//        Book book = booksRepository.getById(bookId);
        Optional<Book> book = booksRepository.findById(bookId);
        if (book.isEmpty()) return;
        List<Purchase> carts = purchasesRepository.findAllByAccountAndDateNull(account);
        List<Book> books;
        Purchase cart;
        if (carts.isEmpty()) {
            cart = new Purchase();
            cart.setAccount(account);
            books = new ArrayList<>();
        } else {
            cart = carts.get(0);
            books = cart.getBooks();
        }
            books.add(book.get());
            cart.setBooks(books);
            purchasesRepository.save(cart);
    }

    public void cartToOrder(Account account) {
        List<Purchase> carts = purchasesRepository.findAllByAccountAndDateNull(account);
        Purchase cart;
        if (!carts.isEmpty()) {
            cart = carts.get(0);
            cart.setDate(new Date());
            purchasesRepository.save(cart);
        }
    }
}
