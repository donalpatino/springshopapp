package ru.alpatov.SpringShop.util;

import net.datafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.models.Book;
import ru.alpatov.SpringShop.models.Purchase;
import ru.alpatov.SpringShop.repositories.AccountsRepository;
import ru.alpatov.SpringShop.repositories.BooksRepository;
import ru.alpatov.SpringShop.repositories.PurchasesRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author Viacheslav Alpatov
 */
@Component
public class InitDB {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AccountsRepository accountsRepository;
    @Autowired
    BooksRepository booksRepository;

    @Autowired
    PurchasesRepository purchasesRepository;

    @PostConstruct
    @Transactional
    public void init()  {

        Account adminAccount = Account.builder()
                .name("Admin")
                .lastName("Adminov")
                .email("a@a.ru")
                .password(passwordEncoder.encode("Qwert123"))
                 .dateOfBirth(new Date("02/16/1975"))
                .createdAt(LocalDateTime.now())
                .role("ROLE_ADMIN")
                .build();
        accountsRepository.save(adminAccount);

        Account userAccount = Account.builder()
                .name("User")
                .lastName("Useroff")
                .email("u@u.ru")
                .password(passwordEncoder.encode("Qwert123"))
                 .dateOfBirth(new Date ("04/30/1970"))
                .createdAt(LocalDateTime.now())
                .role("ROLE_USER")
                .build();
        accountsRepository.save(userAccount);

        Book book1 = Book.builder()
                .name("Чапаев и Пустота")
                .author("Виктор Пелевин")
                .yearOfPublish(1996)
                .price(500)
                .build();
        booksRepository.save(book1);

        Book book2 = Book.builder()
                .name("KGBT+")
                .author("Виктор Пелевин")
                .yearOfPublish(2022)
                .price(1500)
                .build();
        booksRepository.save(book2);

        Book book3 = Book.builder()
                .name("Generation «П»")
                .author("Виктор Пелевин")
                .yearOfPublish(1999)
                .price(400)
                .build();
        booksRepository.save(book3);

        Book book4 = Book.builder()
                .name("Сердце Пармы")
                .author("Алексей Иванов")
                .yearOfPublish(2003)
                .price(1500)
                .build();
        booksRepository.save(book4);

        Book book5 = Book.builder()
                .name("Блуда и МУДО")
                .author("Алексей Иванов")
                .yearOfPublish(2007)
                .price(150)
                .build();
        booksRepository.save(book5);

        Purchase purchase = Purchase.builder()
                .date(new Date())
                .account(userAccount)
                .books(List.of(book1,book4))
                .build();
        purchasesRepository.save(purchase);

        Purchase cart = Purchase.builder()
                .account(userAccount)
                .books(List.of(book1,book4))
                .build();
        purchasesRepository.save(cart);

        Faker faker = new Faker();
        for(int i =0; i<100; i++ ) {
            Book book = Book.builder()
                    .name(faker.book().title())
                    .author(faker.book().author())
                    .yearOfPublish(getRandomNumberUsingNextInt(1800,2010))
                    .price(getRandomNumberUsingNextInt(100,5000))
                    .build();
            booksRepository.save(book);
        }
    }
    private int getRandomNumberUsingNextInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }
}
