package ru.alpatov.SpringShop.util;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.alpatov.SpringShop.dto.AccountCreationDto;
import ru.alpatov.SpringShop.models.Account;
import ru.alpatov.SpringShop.repositories.AccountsRepository;
import ru.alpatov.SpringShop.services.AccountDetailsService;
import ru.alpatov.SpringShop.services.AccountService;

/**
 * @author Viacheslav Alpatov
 */
@Component
@RequiredArgsConstructor
public class AccountValidator implements Validator {

    @Autowired
    private final AccountDetailsService accountDetailsService;
    @Autowired
    private final AccountService accountService;
    @Override
    public boolean supports(Class<?> aClass) {
        return Account.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        AccountCreationDto accountCreationDto = (AccountCreationDto) o;
        //А вот она проверка на уникальность email
/*        try {
            accountDetailsService.loadUserByUsername(accountCreationDto.getEmail());
        } catch (UsernameNotFoundException ignored) {
            return; // все ок, пользователь не найден
        }

        errors.rejectValue("email", "", "Человек с таким email уже существует");*/

        if (accountService.findByEmail(accountCreationDto.getEmail()).isPresent()) {
            errors.rejectValue("email", "", "Человек с таким email уже существует");
        }
    }
}
