package ru.alpatov.SpringShop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Viacheslav Alpatov
 */

/**
 * A DTO for the {@link ru.alpatov.SpringShop.models.Book} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookDto implements Serializable {
    @NotEmpty(message = "Название книги не должно быть пустым")
    @Size(min = 2, max = 100, message = "Имя должно быть от 2 до 100 символов длиной")
    private String name;
    @NotEmpty(message = "Автор книги должен быть указан")
    @Size(min = 2, max = 100, message = "Имя должно быть от 2 до 100 символов длиной")
    private String author;
    @Positive
    @Min(value = 1900, message = "Год публикации должен быть больше, чем 1900")
    private int yearOfPublish;
    @Positive
    @Min(value = 100, message = "Цена не может быть меньше 100 рублей")
    private int price;
    private List<PurchaseDto> purchases;

    @Override
    public String toString() {
        return "BookDTO{" +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", yearOfPublish=" + yearOfPublish +
                '}';
    }
}
