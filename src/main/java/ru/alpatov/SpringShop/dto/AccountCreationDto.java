package ru.alpatov.SpringShop.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Viacheslav Alpatov
 */

/**
 * A DTO for the {@link ru.alpatov.SpringShop.models.Account} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountCreationDto implements Serializable {
    @NotEmpty(message = "Email не должен быть пустым")
    @Email
    @Size(min = 2, max = 100, message = "Имя должно быть от 2 до 100 символов длиной")
    private String email;
    @NotEmpty(message = "Имя не должно быть пустым")
    @Size(min = 2, max = 100, message = "Имя должно быть от 2 до 100 символов длиной")
    private String name;
    @NotEmpty(message = "Фамилия не должна быть пустым")
    @Size(min = 2, max = 100, message = "Имя должно быть от 2 до 100 символов длиной")
    private String lastName;
    private Date dateOfBirth;
    private String password;
}


