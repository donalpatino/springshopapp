import requests
from  spring_auth import spring_auth
sess = requests.Session()

id = 3
URL = f"http://localhost:8080/api/books/{id}"

r = spring_auth(sess).get(url = URL)

book = r.json()
print(f"Название книги: {book['name']}")
#print(f"Название книги: {book.author}")