import requests
from  spring_auth import spring_auth
sess = requests.Session()

URL = f"http://localhost:8080/api/books"

data = {
    "name": "Властелин колец",
    "author": "Толкин",
    "yearOfPublish": "1950",
    "price": "1000"
}

r = spring_auth(sess).post(url = URL,json=data)

if r.status_code == 200:
    print ("Книга успешно создана")
else:
    print(r.text)