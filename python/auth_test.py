import re
import requests
from bs4 import BeautifulSoup

sess = requests.Session()
#Эмулируем заход через login page
URL1 = 'http://localhost:8080/signin'
r =sess.get(URL1)  # sets cookie

#Смотрим в какое поле пишется csrf токен
#print(r.content)

#Забираем csrf токен и куки
soup = BeautifulSoup(r.text, "html.parser")
csrfToken = soup.find('input',attrs = {'name':'_csrf'})['value']
cookies = r.cookies

#Эмулируем логин

URL2 = 'http://localhost:8080/process_login'
data = {
    "username": "a@a.ru",
    "password": "Qwert123",
    "_csrf": csrfToken
}
headers = {'Content-Type': 'application/x-www-form-urlencoded'}
r = sess.post(URL2, data=data, headers=headers)

print(r.content)
print("========================Проверяем доступной страниц внутри==============")

URL3= f"http://localhost:8080/admin"
r = sess.get(url = URL3)
print (r.content)

print("========================Проверяем доступной страниц внутри==============")

URL3= f"http://localhost:8080/admin/users"
r = sess.get(url = URL3)
print (r.content)


#И обламываемся с POST на REST
URL4 = f"http://localhost:8080/api/users"

data = {
    "name": "John",
    "lastName": "Facker",
    "email": "john@fuck.com",
    "dateOfBirth": "2000-10-30",
    "password": "123"
}

r = sess.post(url = URL4,json=data)

if r.status_code == 200:
    print ("Пользователь успешно создан")
else:
    print(r.text)
