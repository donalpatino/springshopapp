import requests
from bs4 import BeautifulSoup

def spring_auth(sess): 
    #Эмулируем заход через login page
    URL1 = 'http://localhost:8080/signin'
    r =sess.get(URL1)  # sets cookie

    #Смотрим в какое поле пишется csrf токен
    #print(r.content)

    #Забираем csrf токен и куки
    soup = BeautifulSoup(r.text, "html.parser")
    csrfToken = soup.find('input',attrs = {'name':'_csrf'})['value']
    cookies = r.cookies

    #Эмулируем логин

    URL2 = 'http://localhost:8080/process_login'
    data = {
        "username": "a@a.ru",
        "password": "Qwert123",
        "_csrf": csrfToken
    }
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    r = sess.post(URL2, data=data, headers=headers)
    print("========================Проверяем успех авторизации==============")
    print(r.content)
    return sess


